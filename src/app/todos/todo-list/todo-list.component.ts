import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Todo } from '../todo';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent {

  /*
    This property is bound using its original name. 
    But we could bound this variable to diffrent property name 
    from component template, e.g.: 
      @Input('tasks') todos: Todo[];
      ... and in template:
      <app-todo-list tasks="todos"></app-todo-list>
  */
  @Input()
  todos: Todo[];

  @Output()
  remove: EventEmitter<Todo> = new EventEmitter();

  @Output()
  toggleComplete: EventEmitter<Todo> = new EventEmitter();

  constructor() {}

  onRemoveTodo(todo: Todo) {
    this.remove.emit(todo);
  }

  onToggleCompleteTodo(todo: Todo) {
    this.toggleComplete.emit(todo);
  }

}
