import { Component, OnInit } from '@angular/core';
import { Todo } from './todo';
import { TodoDataService } from './todo-data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss']
})
export class TodosComponent implements OnInit {

  todos: Todo[] = [];
  todosLeft: number;

  constructor(
    private todoDataService: TodoDataService,
    private route: ActivatedRoute,
    private auth: AuthService,
    private router: Router) {}
    
  ngOnInit() {
    this.route.data.pipe(
      map(data => data.todos)
    ).subscribe(
      (todos) => {
        this.todos = todos;
      }
    );

    this.todosLeft = this.todos.filter(todo => !todo.complete).length;
  }

  onAddTodo(todo: Todo) {
    this.todoDataService
      .addTodo(todo)
      .subscribe(newTodo => {
        this.todos.push(newTodo);
        this.updateTodosLeft();
      });
  }

  onToggleTodoComplete(todo: Todo) {
    this.todoDataService
      .toggleTodoComplete(todo)
      .subscribe(updatedTodo => { 
        todo = updatedTodo;
        this.updateTodosLeft();
      });
  }

  onRemoveTodo(todo: Todo) {
    this.todoDataService
      .deleteTodoById(todo.id)
      .subscribe(() => { 
        this.todos = this.todos.filter((t: Todo) => todo.id !== t.id);
        this.updateTodosLeft();
      });
  }

  getTodos(): Todo[] {
    return this.todos;
  }

  onSignOut() {
    this.auth.signOut();
    this.router.navigate(['sign-in']);
  }

  private updateTodosLeft() {
    this.todosLeft = this.todos.filter(todo => !todo.complete).length;
  }
}
