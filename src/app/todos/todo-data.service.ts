import { Injectable } from '@angular/core';
import { Todo } from './todo'
import { ApiService } from '../api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TodoDataService {

  constructor(private api: ApiService) { }

  addTodo(todo: Todo): Observable<Todo> {
    return this.api.postNewTodo(todo);
  }
  
  deleteTodoById(id: number): Observable<{}> {
    return this.api.deleteTodoById(id);
  }

  updateTodo(todo: Todo): Observable<Todo> {
    return this.api.updateTodo(todo);
  }

  getTodos(): Observable<Todo[]> {
    return this.api.getAllTodos();
  }

  getTodoById(id: number): Observable<Todo> {
    return this.api.getTodoById(id);
  }

  toggleTodoComplete(todo: Todo): Observable<Todo> {
    todo.complete = !todo.complete;

    return this.api.updateTodo(todo);
  }

}
