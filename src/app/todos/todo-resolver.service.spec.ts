import { TestBed } from '@angular/core/testing';

import { TodoResolverService } from './todo-resolver.service';
import { TodoDataService } from './todo-data.service';
import { ApiService } from '../api.service';
import { ApiMockService } from '../api-mock.service';

describe('TodoResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      TodoDataService,
      {
        provide: ApiService,
        useClass: ApiMockService
      }
    ]
  }));

  it('should be created', () => {
    const service: TodoResolverService = TestBed.get(TodoResolverService);
    expect(service).toBeTruthy();
  });
});
