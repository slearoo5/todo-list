import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../api.service';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  public signInForm: FormGroup;

  public isBusy = false;
  public hasFailed = false;
  public showInputErrors = false;

  constructor(
    private api: ApiService,
    private auth: AuthService,
    private builder: FormBuilder,
    private router: Router
  ) { }

  ngOnInit() {
    this.signInForm = this.builder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  onSignIn() {
    if (this.signInForm.invalid) {
      this.showInputErrors = true;
      return;
    }

    this.isBusy = true;
    this.hasFailed = false;
    
    // Two ways of getting forms value
    const username = this.signInForm.value.username;
    const password = this.signInForm.value.password;

    this.api
      .signIn(username, password)
      .subscribe((response) => {
        this.auth.signIn(response.token, response.name);

        this.router.navigate(['todos']);
      }, (error) => {
        this.isBusy = false;
        this.hasFailed = true;
      })
  }

}
