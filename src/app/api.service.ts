import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Todo } from './todos/todo';
import { environment } from '../environments/environment';
import { Observable } from 'rxjs';

import { SessionService } from './session.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private static readonly API_URL = environment.apiUrl;
  private static readonly API_TODO_URL = environment.apiUrl + '/todos';

  constructor(
    private http: HttpClient,
    private session: SessionService) { }

  public signIn(username: string, password: string): Observable<any> {
    return this.http.post(ApiService.API_URL + '/sign-in', {
      username,
      password
    });
  }

  public getAllTodos(): Observable<Todo[]> {
    const options = this.getHttpOptions();

    return this.http.get<Todo[]>(ApiService.API_TODO_URL, options);
  }

  public getTodoById(id: number): Observable<Todo> {
    const options = this.getHttpOptions();

    return this.http.get<Todo>(ApiService.API_TODO_URL + `/${id}`, options);
  }

  public postNewTodo(todo: Todo): Observable<Todo> {
    const options = this.getHttpOptions();

    return this.http.post<Todo>(ApiService.API_TODO_URL, todo, options);
  }

  public updateTodo(todo: Todo): Observable<Todo> {
    const options = this.getHttpOptions();

    return this.http.put<Todo>(ApiService.API_TODO_URL + `/${todo.id}`, todo, options);
  }

  public deleteTodoById(id: number): Observable<{}> {
    const options = this.getHttpOptions();

    return this.http.delete(ApiService.API_TODO_URL + `/${id}`, options);
  }

  private getHttpOptions(): object {
    const headers: HttpHeaders = new HttpHeaders({
      'Authorization': 'Bearer ' + this.session.getToken
    });
    
    return { headers };
  }
  
}
