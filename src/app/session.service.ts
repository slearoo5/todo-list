import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  private accessToken: string;
  private name: string;

  constructor() { }

  public destroy() {
    this.accessToken = null;
    this.name = null;
  }

  get getToken(): string {
    return this.accessToken;
  }

  get getUsername(): string {
    return this.name;
  }

  set token(token: string) {
    this.accessToken = token;
  }

  set username(name: string) {
    this.name = name;
  }
}
