import { Injectable } from '@angular/core';
import { SessionService } from './session.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private session: SessionService) { }

  public isSignedIn(): boolean {
    return !!this.session.getToken;
  }

  public signOut() {
    this.session.destroy();
  }

  public signIn(accessToken: string, username: string) {
    if ((!accessToken) || (!username)) {
      return
    }
    this.session.token = accessToken;
    this.session.username = username;
  }
}
