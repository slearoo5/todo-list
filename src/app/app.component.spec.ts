import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { TodoListHeaderComponent } from './todos/todo-list-header/todo-list-header.component';
import { TodoListComponent } from './todos/todo-list/todo-list.component';
import { ItemComponent } from './todos/todo-list/item/item.component';
import { TodoListFooterComponent } from './todos/todo-list-footer/todo-list-footer.component';
import { FormsModule } from '@angular/forms';
import { ApiService } from './api.service';
import { ApiMockService } from './api-mock.service';
import { TodoDataService } from './todos/todo-data.service';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        FormsModule
      ],
      declarations: [
        AppComponent,
        TodoListHeaderComponent,
        TodoListComponent,
        ItemComponent,
        TodoListFooterComponent
      ],
      providers: [
        TodoDataService,
        {
          provide: ApiService,
          useClass: ApiMockService
        }
      ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
