# ToodoApp

Simple list-making application created in Angular. It consists of some basic features like:

- fetching data (tasks) from REST API (using Angular HttpClient)
- signing in mechanism and preventing unauthorized user to access private data
- utilizing fake backend (json-server)

## Development server

Run `ng serve -o` for a dev server. This command will automatically open browser and navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Run fake backend

Run `npm run json-server` to simulate communication with external server. This will allow to perform CRUD operation on application data.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
